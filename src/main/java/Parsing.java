import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import static java.time.temporal.ChronoUnit.DAYS;

public class Parsing {


    public static void write(String str){
        try {
            String filepath = System.getProperty("user.dir") + File.separator +"src/main/resources/parsing.dgs";
           FileWriter fw = new FileWriter(filepath);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(str);

            bw.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /*extract all the lines from the excel file*/
    public static LinkedList<Trajet> parsing(File file) {

        LinkedList<Trajet> trajets=new LinkedList<>();
        try{
        // Create a FileInputStream object
        // for getting the information of the file
        FileInputStream fip = new FileInputStream(file);

        // Getting the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook(fip);

            // get the number of sheets
            int number= workbook.getNumberOfSheets();
            String str="";


            for(int i=0;i<number;i++){
                Sheet firstSheet = workbook.getSheetAt(i);

                // Iterators to traverse over
                Iterator<Row> iterator = firstSheet.iterator();
                int rows =0;
                while (iterator.hasNext()) {

                    Row nextRow = iterator.next();
                    if(rows!=0){

                        Iterator<Cell> cellIterator = nextRow.cellIterator();
                        String portDepart="",portArrive="";
                        Date sailedTime=null, arrivalTime=null;
                        String shapeName="";
                        double cout_CO2=100;
                        int cell=0;
                        while (cellIterator.hasNext()) {
                            Cell nextCell = cellIterator.next();
                            if(cell==1){
                                shapeName=nextCell.toString();
                            }
                            if(cell==3){
                                portDepart=nextCell.toString();
                            }
                            if(cell==4){
                                portArrive=nextCell.toString();
                            }
                            if(cell==5){

                                arrivalTime=nextCell.getDateCellValue();
                            }
                            if(cell==7){
                                sailedTime=nextCell.getDateCellValue();
                            }
                            cell++;
                        }
                        trajets.add(new Trajet(shapeName,portDepart,portArrive,cout_CO2,sailedTime,arrivalTime));

                    }//if row
                    rows++;
                   // System.out.println(rows);
                }

            }

        // Ensure if file exist or not
        if (file.isFile() && file.exists()) {
            System.out.println("Geeks.xlsx open");
        }
        else {
            System.out.println("Geeks.xlsx either not exist"
                    + " or can't open");
        }
            workbook.close();
            fip.close();

    }catch (Exception e){
        }

        return trajets;
    }



    public static <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }



    /*

     */
    public static LinkedList port2(List<Trajet> trajets){

        //Put all the ports in a List
        List<String> allPorts=new LinkedList<>();
        allPorts.add("Brest");
        allPorts.add("LeHavre");
        allPorts.add("Bassens");
        allPorts.add("Montoir");
        allPorts.add("Antwerp");
        allPorts.add("Tanger-Med");
        allPorts.add("LaRochelle-Pallice");
        allPorts.add("Rotterdam");
        allPorts.add("Algericas");
        allPorts.add("Shangai");


        /*Put all the previous ports in a string with a DGS format */
        String s="DGS004\n" +
                "triangled 0 37\n\n\n";
        for(int i=0; i < allPorts.size();i++){
             s+="an "+allPorts.get(i) +" ui.label:"+allPorts.get(i) +" port:"+allPorts.get(i) +" ";
             if (i==0){
                 s+="source:"+allPorts.get(0);
             }
             if (allPorts.get(i).equals("Shangai")){
                 s+="destination:Shanghai ";
             }
             s+="\n";
        }


        /*this function ask the user to enter the year and the month */
        int donnees[]=mois();
        int annee=donnees[0];
        int moisD=donnees[1];
        //int moisF=donnees[2];


     /*and here, we ask the user to type on a keyboard the deadline(number max of days to reach Shangai,
       then we calculate  date fin =datedebut + datelimit*/
        Scanner zz=new Scanner(System.in);
        Date dateDebut =  new GregorianCalendar(annee, moisD, 1).getTime();
        System.out.println("Saisissez  la journee limite ");
        int dateLimite = zz.nextInt() - 1;//car le commence le 1
        long finEtude = dateDebut.getTime()+ TimeUnit.DAYS.toMillis(dateLimite);
        Date dateFin=new Date(finEtude);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateFin);
        final int moisF = cal.get(Calendar.MONTH);
        int anneeF=cal.get(Calendar.YEAR);
    //---------------


        /*Loop over the list of ports*/
        String arete="\n\n";
        int arreteNum=0;
        for(int i=0; i < allPorts.size();i++){
            for(int j=0; j < allPorts.size();j++){

                /*avoid spaces on ports' names (le Havre ---> lehavre)*/
                if(!allPorts.get(i).replaceAll("\\s+","").equals(allPorts.get(j).replaceAll("\\s+",""))){
                    List<Trajet> tr=null;
                    HashMap<Trajet,List<Integer>> trajetDatedeDepart=new  HashMap();
                    final int jj=j, ii=i;


                    /*extract from the excel file all the lignes(trajet) between dateBegin and dateEnd*/
                    tr=trajets.stream().filter((a)->a.getPortDepart().equals(allPorts.get(ii)) && a.getPortArrive().equals(allPorts.get(jj)))
                            .filter(e->((e.getSailedTime().getTime() > dateDebut.getTime() && (dateFin.getTime() > e.getSailedTime().getTime()) )
                            || ((e.getArrivalTime().getTime() > dateDebut.getTime()) && (dateFin.getTime() > e.getArrivalTime().getTime())) ))
                            .collect(Collectors.toList());

                    /*For each trajet, search it's duration, exemple :
                     Brest -> le havre sailedTime=01/01/2019 shape=001 ,
                     so we look in the of le havre, we extract all arrival times which correspend to the shape 001,
                     then we filter arrival time > (sailedTime=01/01/2019), after that that we take the min */
                    for(Trajet t:tr){
                        List<Long> minDuree=trajets.stream().filter(e->e.getPortDepart().equals(t.getPortArrive()) && t.getShapeName().equals(e.getShapeName())
                                && e.getArrivalTime().getTime() > t.getSailedTime().getTime()   && e.getArrivalTime().getTime() <= dateFin.getTime() )
                                .map(e-> DAYS.between(Trajet.removeInfoDate(t.getSailedTime()).toInstant(), Trajet.removeInfoDate(e.getArrivalTime()).toInstant()))
                                .collect(Collectors.toList());
                        if(minDuree.size()!=0) {
                            t.setDuree(Collections.min(minDuree));
                        }
                    }
                    //-------------------------------------------




                    /*and here, we extract the dates de depart corresponding to each trajet,
                    if arrival time is not found ( duree=-1) then delete it */
                    List<Long> durees= tr.stream().map(e->e.getDuree()).distinct().collect(Collectors.toList());
                    if(tr.size()>0) {
                        for (long x : durees) {
                            final long xx = x;
                            List<Trajet> trCopie=tr.stream().filter(e -> e.getDuree() == xx && e.getDuree()!=-1).collect(Collectors.toList());
                            if(trCopie.size()>0) {
                                trajetDatedeDepart.put(trCopie.get(0), trCopie.stream().map(e -> {
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(e.getSailedTime());
                                    Date dateDebutEtude =  new GregorianCalendar(annee, moisD, 1).getTime();
                                    return  (int)DAYS.between(Trajet.removeInfoDate(dateDebutEtude).toInstant(), Trajet.removeInfoDate(e.getSailedTime()).toInstant())+1;
                                    //----------------
                                }).distinct().collect(Collectors.toList()));
                            }
                        }

                        /*add the trajets to the String (after transforming them to the DGS format)  */
                        Random rand=new Random();
                        for (Trajet name : trajetDatedeDepart.keySet()) {
                            Trajet trajet = name;
                            List value = trajetDatedeDepart.get(name);
                            String dateDeparts = value.toString().replaceAll("\\s+","");
                            arete+="ae e" +(arreteNum+=1) + " "+trajet.getPortDepart().replaceAll("\\s+","")+" > "+trajet.getPortArrive().replaceAll("\\s+","")+
                                    " durée:"+trajet.getDuree()+ " cout_co2:"+rand.nextInt(200);

                            if(value.size()==1){
                                arete+=" dates_departs:"+dateDeparts.substring(1, dateDeparts.length()-1)+" seul:oui";
                            }
                            if(value.size()>1){
                                arete+=" dates_departs="+dateDeparts.substring(1, dateDeparts.length()-1);
                            }
                            arete+="\n";
                        }
                    }

                }
            }
        }


        arete+="ae e1000 Antwerp > Shangai durée:2 cout_co2:11 dates_departs=1,3,9,12,15,18,21,24,27,30,33\n" +
                "ae e1001 Rotterdam  > Shangai durée:3 cout_co2:20 dates_departs=1,3,9,12,15,18,21,24,27,30,33\n" +
                "ae e1002 LeHavre  > Shangai durée:4 cout_co2:10 dates_departs=1,7,14,21,28\n" +
                "ae e1003 Algericas  > Shangai durée:4 cout_co2:18 dates_departs=1,4,8,12,16,20,24\n"+
                "ae e1004 Tanger-Med  > Shangai durée:6 cout_co2:30 dates_departs=1,4,8,12,16,20,24\n"+
                "ae e1005 Tanger-Med  > Shangai durée:10 cout_co2:50 dates_departs=1,4,8\n"+
                "ae e10001 Tanger-Med  > Shangai durée:15 cout_co2:5 dates_departs=6,8\n"+
                "ae e1006 Brest  > Tanger-Med durée:5 cout_co2:40 dates_departs=1,4,8\n"+
                "ae e1007 Brest  > Tanger-Med durée:10 cout_co2:30 dates_departs=2,10,15\n";


        /*put the string in the dgs file*/
        write(s+arete);


        LinkedList list=new LinkedList();
        list.add(dateDebut);
        list.add(dateLimite);
        return list;
    }



    //ask the user to enter annee and date de debut
    public static int[] mois(){
        Scanner zz= new Scanner(System.in);
        System.out.println("Saisissez l'annee d'etude :");
        int annee=zz.nextInt();
        System.out.println("Saisissez le mois de debut [0-12]:");
        System.out.println("Le mois de debut: ");
        int moisD = zz.nextInt();
        int[] a={annee,moisD};
        return a;
    }


}

