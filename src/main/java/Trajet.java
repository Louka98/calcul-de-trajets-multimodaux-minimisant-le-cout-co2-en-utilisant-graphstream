import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.DAYS;

public class Trajet {
    public Trajet(String shapeName,String portDepart, String portArrive, double cout_CO2, Date dateDepart, Date dateArrive) {
        this.portDepart = portDepart;
        this.portArrive = portArrive;
        this.cout_CO2 = cout_CO2;
        this.sailedTime = dateDepart;
        this.arrivalTime = dateArrive;
        this.duree=-1;
        this.shapeName = shapeName;
    }

    private String portDepart,portArrive;
   private double cout_CO2;
   private Date sailedTime, arrivalTime;
    private long duree;


    public String getShapeName() {
        return shapeName;
    }

    public void setShapeName(String shapeName) {
        this.shapeName = shapeName;
    }

    private String shapeName;

    public long getDuree() {
        return this.duree;
    }

    public static Date removeInfoDate(Date d){
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.MILLISECOND,0);
        cal.set(Calendar.SECOND,0);
        return  cal.getTime();
    }


    public static long dureeDiff(Date d1,Date d2) {
        long dureeMili=d2.getTime() - d1.getTime();
        return TimeUnit.MILLISECONDS.toDays(dureeMili);
        //return  DAYS.between(removeInfoDate(d1).toInstant(), removeInfoDate(d2).toInstant());
    }

    public void setDuree(long d) {
        this.duree = d;;
    }


    public String getPortDepart() {
        return portDepart;
    }

    public void setPortDepart(String portDepart) {
        this.portDepart = portDepart;
    }

    public String getPortArrive() {
        return portArrive;
    }

    @Override
    public String toString() {
        return "Trajet{" +
                "portDepart='" + portDepart + '\'' +
                ", portArrive='" + portArrive + '\'' +
                ", cout_CO2=" + cout_CO2 +
                ", dateDepart=" + sailedTime +
                ", DateArrive=" + arrivalTime +
                ", duree=" + duree +
                '}';
    }

    public int moisNumber(Date date){
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return cal.get(Calendar.MONTH);
    }

    public void setPortArrive(String portArrive) {
        this.portArrive = portArrive;
    }

    public double getCout_CO2() {
        return cout_CO2;
    }

    public void setCout_CO2(double cout_CO2) {
        this.cout_CO2 = cout_CO2;
    }

    public Date getSailedTime() {
        return sailedTime;
    }

    public void setSailedTime(Date sailedTime) {
        this.sailedTime = sailedTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }


}
