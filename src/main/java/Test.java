import org.graphstream.algorithm.BellmanFord;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.GraphParseException;
import org.graphstream.ui.layout.Layout;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerPipe;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Test {

    private Graph graph, copie;

    public Test(){
        this.graph = new MultiGraph("test");

        try {
            graph.read("src/main/resources/parsing.dgs");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (GraphParseException e) {
            e.printStackTrace();
        }
        this.copie=this.graph;
    }




    /*creer un graphe qui contient (nombre de ports * nombre de journees) sommets
    ces sommets ont les attribut suivantes : port,rang,jours
     Danc on aura les sommets suivants
     (Le havre, 1,1),(Le havre, 1,2)............(Le havre, 1,30),(Le havre, 1,31)
      (Rouen, 2,1),(Rouen, 2,2)............(Rouen, 2,30),(Rouen, 2,31)
      ...
      ...
      ...
      (Shangai, 7,1),(Shangai, 7,2)............(Shangai, 7,30),(Shangai, 7,31)*/

    public void createNodes( int periode) {
        Graph g1 = new DefaultGraph("test0");
        int count = 2, countNodes = this.copie.getNodeCount(), x = 1, y = 20;
        for (Node node : this.copie.getEachNode()) {

            String port = node.getAttribute("port");

            if (node.hasAttribute("source")) {//source

                for (int i = 1; i <= periode; i++) {
                    g1.addNode(1 + "" + i);
                    Node n = g1.getNode("1" + i);
                    n.setAttribute("jours", i);
                    n.addAttribute("rang", 1);
                    n.addAttribute("port", port);
                    n.addAttribute("source", "");
                }
            } else {
                if (node.hasAttribute("destination")) {//destination
                    x = 1;                    for (int i = 1; i <= periode; i++) {

                        g1.addNode(countNodes + "" + i);
                        Node n = g1.getNode(countNodes + "" + i);
                        n.addAttribute("jours", i);
                        n.addAttribute("rang", countNodes);
                        n.addAttribute("port", port);
                        n.addAttribute("ui.style", "fill-color: blue;size: 15px, 20px;");
                        n.addAttribute("destination", "");

                    }
                } else {//autres
                    for (int i = 1; i <= periode; i++) {
                        g1.addNode(count + "" + i);
                        Node n = g1.getNode(count + "" + i);
                        n.addAttribute("jours", i);
                        n.addAttribute("rang", count);
                        n.addAttribute("port", port);
                    }
                    count++;
                }
            }
        }
        this.graph=g1;
    }

    //ajouter des aretes entre les sommets du meme port(meme rang)
    public void ajoutesAretes( int ligne, int periode) {
        int rang = 1;
        while (rang <= ligne) {
            for (int i = 1; i <= periode - 1; i++) {

                Node n = this.graph.getNode(rang + "" + i);
                n.addAttribute("jours", i);
                this.graph.addEdge(rang + "" + i, n, this.graph.getNode(rang + "" + (i + 1)), true).addAttribute("cout", 0.0);
                Edge edge = this.graph.getEdge(rang + "" + i);
                edge.addAttribute("durée", 1.0);

            }
            rang++;
        }
    }
    //
//
    public void css( int ligne, int periode) {
        int rang = 1, x = 0, y = 1000;
        while (rang <= ligne) {
            for (int i = 1; i <= periode; i++) {

                Node n = this.graph.getNode(rang + "" + i);
                int journee = n.getAttribute("jours");
                String port = n.getAttribute("port");
                n.setAttribute("ui.label", port);

                if( journee==1){
                    n.setAttribute("ui.label", port.charAt(0) + "," + journee);
                }else{
                    n.setAttribute("ui.label", ""+ journee);
                }

                n.addAttribute("xy", x += 80, y);

                if (rang == 1) {
                    n.addAttribute("ui.style", "fill-color: gray;size: 25px, 22px;");
                } else {
                    if (rang == ligne) {
                        n.addAttribute("ui.style", "fill-color: red;size: 25px, 22px;");
                    } else {
                        n.addAttribute("ui.style", "fill-color: gray;size: 25px, 22px;");
                    }
                }
            }
            rang++;
            x = 10;
            y -= 130;
        }

    }

    //    /*
//    construire un graphe etendu//     */
    public void etendu() {
        Graph etendu = this.graph;

        for (Node node : this.copie.getEachNode()) {
            String portSource = node.getAttribute("port");

            for (Edge e : node.getEachLeavingEdge()) {
                int duree = e.getAttribute("durée");
                double co2 = (int) e.getAttribute("cout_co2");
                Object[] datesDeparts = null;

                if(e.hasAttribute("seul")) {
                    Integer date = e.getAttribute("dates_departs");
                    Integer[] da = {date};
                    datesDeparts = da;
                }else{
                    datesDeparts=e.getAttribute("dates_departs");
                }

                Node destination = e.getTargetNode();
                String portDestination = destination.getAttribute("port");
                for (int i = 0; i < datesDeparts.length; i++) {
                    Node source = null, destinationFinal = null;

                    for (Node n : etendu.getEachNode()) {

                        if (n.getAttribute("port") == portSource && n.getAttribute("jours") == datesDeparts[i]) {
                            source = n;
                        }

                        int z = (int) datesDeparts[i] + duree;
                        int jours = (int) n.getAttribute("jours");

                        if (n.getAttribute("port") == portDestination && jours == z) {
                            destinationFinal = n;
                        }

                    }
                    Random rand =new Random();
                    etendu.addEdge(source.getAttribute("port") + "" + destinationFinal.getAttribute("port") + "" + duree + "" + (int) datesDeparts[i]+""+rand.nextInt(500), source, destinationFinal, true).addAttribute("cout", co2);
                    // etendu.getEdge(source.getAttribute("port")+""+destinationFinal.getAttribute("port")+""+duree+""+(int)datesDeparts[i]).addAttribute("durée",(double)duree);

                }
            }
        }
        this.graph=etendu;
    }
    //
//
    public static Graph belmanFord() {

        Test test = new Test();
        int countNodes=test.copie.getNodeCount();

        Scanner zz = new Scanner(System.in);

        LinkedList list = Parsing.port2(Parsing.parsing(new File( System.getProperty("user.dir") + File.separator +"src/main/resources/DonnéesescalespourERIC.xlsx")) );
        int dateLimite=(int)list.get(1);
        Date dateDebut=(Date)list.get(0);

        int limit = dateLimite;
        if (dateLimite < max()) limit = max();
        Graph g = organiser(limit);
        System.out.println("Choisissez le port de depart ");
        int a = 0;
        String portNom = "";

        System.out.println("Montoir => 1\n" +
                "Brest => 2\n" +
                "Bassens => 3\n"+
                "LaRochelle-Pallice => 4\n"
        );
        while (a == 0) {
            a = zz.nextInt();
            switch (a) {
                case 1:
                    portNom = "Montoir";
                    break;
                case 2:
                    portNom = "Brest";
                    break;
                case 3:
                    portNom = "Bassens";
                    break;
                case 4:
                    portNom = "LaRochelle-Pallice";
                    break;

                default:
                    System.out.println("Entrer un chiffre entre 1 et 4");
                    a = 0;            }
        }

        System.out.println("Choisissez une date de depart ");
        int dateDepart = zz.nextInt();

        Node portDepart = null;
        for (Node node : g.getEachNode()) {
            String port = node.getAttribute("port");
            String id = node.getId();
            int journee = Integer.parseInt(id.substring(1, id.length()));
            if (port.equals(portNom) && dateDepart == journee) {
                portDepart = node;
            }
        }

        String idDepart = portDepart.getId();

        //changer le css de noeud de depart
        g.getNode(idDepart).addAttribute("ui.style", "fill-color: yellow;size: 25px, 22px;");

        BellmanFord bf = new BellmanFord("cout", idDepart);
        bf.init(g);
        bf.compute();

        //les dates d'arrivées des shortest paths
        LinkedHashMap<Integer,Double> dates =new LinkedHashMap<>();
        dates.put(0,0.0);
        for (int i=1;i<=dateLimite;i++){
            double min = Collections.min(dates.values());
            Double pathValue=bf.getShortestPathValue(g.getNode(countNodes+""+i));
            if (min<pathValue && !pathValue.isInfinite() && !dates.containsValue(pathValue)){
                dates.put(i, pathValue.doubleValue());
            }
        }
        dates.remove(0,0.0);


        //on recupere le chemin(le chemin de la destination vers la source)
        try{
            for (Map.Entry<Integer, Double> entry : dates.entrySet()) {
                int date = entry.getKey().intValue();
                double value = entry.getValue().doubleValue();


                List<Node> path=bf.getShortestPath(g.getNode(countNodes+""+date)).getNodePath();
                Collections.reverse(path);//on fait un reverce (source vers destination)

                String portDestination=path.get(path.size()-1).getAttribute("port");//recuperer la source

                //Bien afficher le chemin
                int j=0;
                for(int i=0;i<path.size();i++){
                    if(path.get(i).getAttribute("port")==portDestination) {
                        j++;
                        if(j>=2){
                            path.remove(i);
                            i--;
                        }
                    }
                }

                //affichage
                if(path.size()==1||path.size()==0){
                    System.out.println("il ya un petit probleme, le navire arrive le mois prochaine ou bien vous avez mis des parametres erronés");
                }else{
                    int h=1;
                    Random rand= new Random();
                    int red= rand.nextInt(255);
                    int green=rand.nextInt(255);
                    int blue=rand.nextInt(255);

                    System.out.println();
                    for(Node n:path){
                        int day=n.getAttribute("jours");
                        //long l=(long)n.getAttribute("jours");
                        Date d=new Date(dateDebut.getTime()+ TimeUnit.DAYS.toMillis(day-1));
                        SimpleDateFormat format =new SimpleDateFormat("dd/MM/yyyy");
                        String strDate= format.format(d);
                        System.out.println(n.getAttribute("port") +" <----> "+strDate);

                        //System.out.println(n.getAttribute("port") +" "+n.getAttribute("jours"));
                        if(path.size()>h)  n.getEdgeBetween(path.get(h)).addAttribute("ui.style", "fill-color: rgb("+red+","+green+","+blue+");size: 7px;");
                        h++;


                        //css des sommets d'arrivées
                        n.addAttribute("ui.style", "fill-color: rgb("+red+","+green+","+blue+");size: 25px, 22px;");;
                    }
                    System.out.println();
                    System.out.println("Cout  CO2 <----> "+value);
                    System.out.println("___________________________________");

                }
            }
        } catch (NullPointerException e) {
            String portDepartNom = portDepart.getAttribute("port");
            System.out.println("Il n'y a aucun chemin entre {" + portDepartNom + "," + dateDepart + "} et " +
                    "{Shangai," + dateLimite + "}");
        }

        return g;

    }


    public static int max() {

        Test test=new Test();

        Object[] datesDeparts = null;
        List<Integer> p=new LinkedList();

        int max=0;

        for (Edge e:test.copie.getEachEdge()) {

            if(e.hasAttribute("seul")) {

                Integer date = e.getAttribute("dates_departs");
                Integer[] da = {date};
                datesDeparts = da;
            }else{
                datesDeparts=e.getAttribute("dates_departs");
            }
            int durée=e.getAttribute("durée");
            List dp=Arrays.asList(datesDeparts).stream().map((k)->((int)k+durée)).collect(Collectors.toList());
            p.addAll(dp);
        }


        return Collections.max(p);
    }

    //
//
    public static Graph organiser(int periode) {
        //System.out.println("Saisissez la date limite d'arrivée ");
        //Scanner zz=new Scanner(System.in);
        //String name=zz.nextLine();
        // int periode=zz.nextInt();

        Test test=new Test();
        test.createNodes(periode);
        test.ajoutesAretes(test.copie.getNodeCount(),periode);
        test.etendu();
        test.css(test.copie.getNodeCount(),periode);


        return test.graph;
    }


    public static void main(String[] args) {
        // System.setProperty("org.graphstream.ui", "swing");
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");

        //test.graph.display();
        //System.out.println(max());
        //Graph etendu = belmanFord();
        //Viewer viewer = test.graph.display();
        //viewer.disableAutoLayout();*//*

        JFrame f = new JFrame("hhh");
        Viewer viewer = new Viewer(belmanFord(), Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
        viewer.disableAutoLayout();


        ViewPanel viewPanel = viewer.addDefaultView(false);
        //viewPanel.setPreferredSize(new Dimension(1350,800));
        viewPanel.setPreferredSize(new Dimension(1350, 800));

        JPanel p = new JPanel();
        JScrollPane scrollFrame = new JScrollPane(p);
        p.setAutoscrolls(true);
        p.add(viewPanel);

        f.add(p);
        f.setSize(1500, 1000);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);

    }
}